# Caddy for armv7 (RPi)

## Run

### Noninteractive setup (recommended)

```
docker run -d --name caddy -v <your-caddyfile>:/etc/caddy/Caddyfile -v <your-caddy-data-dir>:/var/lib/caddy registry.gitlab.com/morph027/caddy-armv7:1.0.4 -conf=/etc/caddy/Caddyfile -email=foo.bar.com -agree
```

### Interactive setup

```
docker run -rm -it -v <your-caddyfile>:/etc/caddy/Caddyfile -v <your-caddy-data-dir>:/var/lib/caddy registry.gitlab.com/morph027/caddy-armv7:1.0.4
Activating privacy features... 

Your sites will be served over HTTPS automatically using Let's Encrypt.
By continuing, you agree to the Let's Encrypt Subscriber Agreement at:
  https://letsencrypt.org/documents/LE-SA-v1.2-November-15-2017.pdf
Please enter your email address to signify agreement and to be notified
in case of issues. You can leave it blank, but we don't recommend it.
  Email address: foo.bar.com
...
```

When done, exit the container and start again in daemon mode:

```
docker run -d --name caddy -v <your-caddyfile>:/etc/caddy/Caddyfile -v <your-caddy-data-dir>:/var/lib/caddy registry.gitlab.com/morph027/caddy-armv7:1.0.4
```