FROM		alpine:latest as builder

ADD		https://caddyserver.com/download/linux/arm7?license=personal&telemetry=off /root/caddy.tgz

RUN		tar -C /root -xzf /root/caddy.tgz

FROM		arm32v7/alpine:latest

COPY		qemu-arm-static /usr/bin
 
RUN		apk --no-cache add \
			sudo \
			libcap

RUN		addgroup -S caddy \
		&& adduser -G caddy -SD -h /var/lib/caddy/ -s /sbin/nologin caddy

COPY		--from=builder /root/caddy /usr/local/bin/caddy

RUN		setcap cap_net_bind_service=+ep /usr/local/bin/caddy

RUN		rm -f /usr/bin/qemu-arm-static

ENTRYPOINT	["/usr/bin/sudo", "-u", "caddy", "-H", "/usr/local/bin/caddy"]

CMD		["-conf", "/etc/caddy/Caddyfile"]